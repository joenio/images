biketrip-france-2021:
	mkdir -p thumbnails/biketrip-france-2021
	mogrify -path thumbnails/biketrip-france-2021 -resize 10% images/biketrip-france-2021/*

biketrip-netherlands-2021:
	mkdir -p thumbnails/biketrip-netherlands-2021
	mogrify -path thumbnails/biketrip-netherlands-2021 -resize 10% images/biketrip-netherlands-2021/*

report-fosdem-2024:
	mkdir -p thumbnails/$@
	mogrify -path thumbnails/$@ -resize 30% images/$@/*

%::
	mkdir -p thumbnails/$@
	mogrify -path thumbnails/$@ -resize 10% images/$@/*
