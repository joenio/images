# images

personal images galleries, trips, day life, art, etc... mainly to be published
on my personal webpage https://joenio.me

## Getting started

Example how to generate thumbnails using ImageMagick:

```sh
mkdir -p thumbnails/biketrip-france-2021
mogrify -path thumbnails/biketrip-france-2021 -resize 10% images/biketrip-france-2021/*
```

Or use `make`:

```sh
make trip-england-2022
```

## License

All images are licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## Copyright

2021-2022, Joenio Marques da Costa
